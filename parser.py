import os
import subprocess
import re
import time
import random
import sys
import urllib
import threading

def _(msg):
    print "[DEBUG][%s] %s"%(time.strftime('%H:%M:%S'), msg)

class JessicaVersion:
    def __init__(self):
        self.SwfName = "Jessica.swf"
        self.Url = 'http://www.transformice.com/forum/%s'%self.SwfName
        self.ActionScript = "%s.as"%self.SwfName

    def parseActions(self):
        subprocess.call(["swfdump", "-a", self.SwfName, ">", self.ActionScript], stdout=open(os.devnull, "w"), stderr=subprocess.STDOUT)
        #os.system("swfdump -a %s > %s"%(self.SwfName, self.ActionScript))
        _("SWF exported as %s"%self.ActionScript)
        fileAS = open(self.ActionScript, "r")
        lines = fileAS.read()
        fileAS.close()
        tokens = {}
        for ip in re.findall('pushstring "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"', lines):
            ip = re.search("pushstring \"(.*?)\"", ip).group(1)
            if ip not in ["127.0.0.1"]:
                tokens["ip"] = ip
                break
        tokens["version"], = re.findall("const <q>\[public\]::VERSION:<q>\[public\]::int = (\d+)\n", lines)
        return tokens

    def Main(self):
        if os.path.exists(self.SwfName): os.remove(self.SwfName)
        _("Fetching SWF")
        urllib.urlretrieve(self.Url, self.SwfName)
        _("SWF fetched")
        return self.parseActions()

def Parse():
    while 1:
        fileq = open("forumVersion.txt", "w+")
        timeStart = time.time()
        forumData = JessicaVersion().Main()
        forumData["updated"] = int(time.time())
        forumData["loadtime"] = time.time()-timeStart
        fileq.write(str(forumData))
        time.sleep(60*60)

    #threading.Timer(60*60, Parse).start()


if __name__ == "__main__":
    print "*"*80 + "JessicaVersion by Devsaider".center(80) + "*"*80
    #Parse()
    print JessicaVersion().Main()
